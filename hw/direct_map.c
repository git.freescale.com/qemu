/*
 * Qemu PowerPC Direct Memory Map support for guests
 *
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc.
 *
 * Author: Scott Wood  <scottwood@freescale.com>
 *         Ashish Kalra <ashish.kalra@freescale.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of  the GNU General  Public License as published by
 * the Free Software Foundation;  either version 2 of the  License, or
 * (at your option) any later version.
 */

#include <dirent.h>
#include <errno.h>
#include <sys/mman.h>

#include "config.h"
#include "qemu-common.h"
#include "kvm.h"
#include "direct_map.h"

#include <libfdt.h>

#define MAX_DT_PATH 256

#define MAX_ADDR_CELLS 4
#define MAX_SIZE_CELLS 2
#define MAX_INT_CELLS 4

#define CELL_SIZE 4

#define ERR_NOTRANS 1

static int get_addr_format_nozero(const void *tree, int node,
                                  uint32_t *naddr, uint32_t *nsize)
{
    *naddr = 2;
    *nsize = 1;

    int len;
    const uint32_t *naddrp = fdt_getprop(tree, node, "#address-cells", &len);
    if (!naddrp) {
        if (len != -FDT_ERR_NOTFOUND)
            return len;
    } else if (len == 4 && *naddrp <= MAX_ADDR_CELLS) {
        *naddr = *naddrp;
    } else {
        return -EINVAL;
    }

    const uint32_t *nsizep = fdt_getprop(tree, node, "#size-cells", &len);
    if (!nsizep) {
        if (len != -FDT_ERR_NOTFOUND)
            return len;
    } else if (len == 4 && *nsizep <= MAX_SIZE_CELLS) {
        *nsize = *nsizep;
    } else {
        return -EINVAL;
    }

    if (*naddr == 0 || *nsize == 0) {
        return -EINVAL;
    }

    return 0;
}

static void copy_val(uint32_t *dest, const uint32_t *src, int naddr)
{
    int pad = MAX_ADDR_CELLS - naddr;

    memset(dest, 0, pad * 4);
    memcpy(dest + pad, src, naddr * 4);
}

static int sub_reg(uint32_t *reg, const uint32_t *sub)
{
    int i, borrow = 0;

    for (i = MAX_ADDR_CELLS - 1; i >= 0; i--) {
        int prev_borrow = borrow;
        borrow = reg[i] < sub[i] + prev_borrow;
        reg[i] -= sub[i] + prev_borrow;
    }

    return !borrow;
}

static int add_reg(uint32_t *reg, const uint32_t *add, int naddr)
{
    int i, carry = 0;

    for (i = MAX_ADDR_CELLS - 1; i >= MAX_ADDR_CELLS - naddr; i--) {
        uint64_t tmp = (uint64_t)reg[i] + add[i] + carry;
        carry = tmp >> 32;
        reg[i] = (uint32_t)tmp;
    }

    return !carry;
}

/* FIXME: It is assumed that if the first byte of reg fits in a
 * range, then the whole reg block fits.
 */
static int compare_reg(const uint32_t *reg, const uint32_t *range,
                       const uint32_t *rangesize)
{
    uint32_t end[MAX_ADDR_CELLS];
    int i;

    for (i = 0; i < MAX_ADDR_CELLS; i++) {
        if (reg[i] < range[i])
            return 0;
        if (reg[i] > range[i])
            break;
    }

    memcpy(end, range, sizeof(end));

    /* If the size forces a carry off the final cell, then
     * reg can't possibly be beyond the end.
     */
    if (!add_reg(end, rangesize, MAX_ADDR_CELLS))
        return 1;

    for (i = 0; i < MAX_ADDR_CELLS; i++) {
        if (reg[i] < end[i])
            return 1;
        if (reg[i] > end[i])
            return 0;
    }

    return 0;
}

/* reg must be MAX_ADDR_CELLS */
static int find_range(const uint32_t *reg, const uint32_t *ranges,
                      int nregaddr, int naddr, int nsize, int buflen)
{
    int nrange = nregaddr + naddr + nsize;
    int i;

    if (nrange <= 0)
        return -EINVAL;

    for (i = 0; i < buflen; i += nrange) {
        uint32_t range_addr[MAX_ADDR_CELLS];
        uint32_t range_size[MAX_ADDR_CELLS];

        if (i + nrange > buflen) {
            return -EINVAL;
        }

        copy_val(range_addr, ranges + i, nregaddr);
        copy_val(range_size, ranges + i + nregaddr + naddr, nsize);

        if (compare_reg(reg, range_addr, range_size))
            return i;
    }

    return -FDT_ERR_NOTFOUND;
}

/* Currently only generic buses without special encodings are supported.
 * In particular, PCI is not supported.  Also, only the beginning of the
 * reg block is tracked; size is ignored except in ranges.
 */
static int xlate_one(uint32_t *addr, const uint32_t *ranges,
                     int rangelen, uint32_t naddr, uint32_t nsize,
                     uint32_t prev_naddr, uint32_t prev_nsize,
                     target_phys_addr_t *rangesize)
{
    uint32_t tmpaddr[MAX_ADDR_CELLS], tmpaddr2[MAX_ADDR_CELLS];
    int offset = find_range(addr, ranges, prev_naddr,
                            naddr, prev_nsize, rangelen / 4);

    if (offset < 0)
        return offset;

    ranges += offset;

    copy_val(tmpaddr, ranges, prev_naddr);

    if (!sub_reg(addr, tmpaddr))
        return -EINVAL;

    if (rangesize) {
        copy_val(tmpaddr, ranges + prev_naddr + naddr, prev_nsize);

        if (!sub_reg(tmpaddr, addr))
            return -EINVAL;

        *rangesize = ((uint64_t)tmpaddr[2]) << 32;
        *rangesize |= tmpaddr[3];
    }

    copy_val(tmpaddr, ranges + prev_naddr, naddr);

    if (!add_reg(addr, tmpaddr, naddr))
        return -EINVAL;

    /* Reject ranges that wrap around the address space.  Primarily
     * intended to enable blacklist entries in fsl,hvranges.
     */
    copy_val(tmpaddr, ranges + prev_naddr, naddr);
    copy_val(tmpaddr2, ranges + prev_naddr + naddr, nsize);

    if (!add_reg(tmpaddr, tmpaddr2, naddr))
        return ERR_NOTRANS;

    return 0;
}

static int xlate_reg_raw(const void *tree, int node, const uint32_t *reg,
                         uint32_t *addrbuf, target_phys_addr_t *size,
                         uint32_t naddr, uint32_t nsize)
{
    uint32_t prev_naddr, prev_nsize;
    const uint32_t *ranges;
    int len, ret;

    int parent = fdt_parent_offset(tree, node);
    if (parent < 0)
        return parent;

    copy_val(addrbuf, reg, naddr);

    if (size) {
        *size = reg[naddr];
        if (nsize == 2) {
            *size = ((uint64_t)*size) << 32;
            *size |= reg[naddr + 1];
        }
    }

    for (;;) {
        prev_naddr = naddr;
        prev_nsize = nsize;
        node = parent;

        parent = fdt_parent_offset(tree, node);
        if (parent == -FDT_ERR_NOTFOUND)
            break;
        if (parent < 0)
            return parent;

        ret = get_addr_format_nozero(tree, parent, &naddr, &nsize);
        if (ret < 0)
            return ret;

        ranges = fdt_getprop(tree, node, "ranges", &len);
        if (!ranges) {
            if (len == -FDT_ERR_NOTFOUND)
                return ERR_NOTRANS;

            return len;
        }

        if (len == 0)
            continue;
        if (len % 4)
            return -EINVAL;

        ret = xlate_one(addrbuf, ranges, len, naddr, nsize,
                        prev_naddr, prev_nsize, NULL);
        if (ret < 0)
            return ret;
    }

    return 0;
}

static int xlate_reg(const void *tree, int node, const uint32_t *reg,
                     target_phys_addr_t *addr, target_phys_addr_t *size,
                     int naddr, int nsize)
{
    uint32_t addrbuf[MAX_ADDR_CELLS];
    int ret;

    ret = xlate_reg_raw(tree, node, reg, addrbuf, size, naddr, nsize);
    if (ret < 0)
        return ret;

    if (addrbuf[0] || addrbuf[1])
        return -EINVAL;

    *addr = ((uint64_t)addrbuf[2] << 32) | addrbuf[3];
    return 0;
}

static void qemu_devtree_get_path(void *tree, int node, char *buf, int buflen)
{
    int ret = fdt_get_path(tree, node, buf, buflen);
    if (ret < 0)
        pstrcpy(buf, buflen, "<couldn't get path>");
}

typedef struct DevMemBlock {
    target_phys_addr_t start, end;
    struct DevMemBlock *next;
} DevMemBlock;

static DevMemBlock *devmem_blocks;

static int devmem_add_block(target_phys_addr_t start, target_phys_addr_t end)
{
    DevMemBlock *block = devmem_blocks;

    while (block) {
        if (block->start <= start && block->end >= end) {
            return -EBUSY;
        }

        block = block->next;
    }

    block = g_malloc(sizeof(DevMemBlock));
    block->start = start;
    block->end = end;
    block->next = devmem_blocks;
    devmem_blocks = block;

    return 0;
}

static int map_mem_from_file(int fd, target_phys_addr_t addr,
                             target_phys_addr_t size, const char *name)
{
    target_phys_addr_t start, end;
    ram_addr_t offset;
    void *ptr;

    if (size == 0)
        return 0;

    start = addr & ~((target_phys_addr_t) qemu_real_host_page_size - 1);
    end = (addr + size + qemu_real_host_page_size - 1) &
          ~((target_phys_addr_t) qemu_real_host_page_size - 1);

    /* The KVM code deals with overlapping memory slots by
     * removing the old slot, registering the new one,
     * and reregistering any parts of the old one that don't
     * overlap.  It can't know that both slots point to the same
     * physical memory, because the host virtual address (and
     * qemu memory "offset") differ.
     *
     * This will not cause breakage, but it does make the region look
     * non-contiguous to KVM and thus it won't use a large page for
     * e.g. the entire CCSR region, if there are subsequent mappings of
     * individual CCSR devices.
     *
     * To avoid that, if the new region is a subset of an already
     * mapped region, don't register it.  Other overlap cases continue
     * to be dealt with by kvm_set_phys_mem.
     */
    if (devmem_add_block(start, end) < 0) {
        return 0;
    }

    ptr = mmap64(NULL, end - start, PROT_READ | PROT_WRITE, MAP_SHARED,
                 fd, start);
    if (ptr == MAP_FAILED) {
        return -errno;
    }

    offset = qemu_ram_alloc_from_ptr(NULL, name, end - start, ptr);
    cpu_register_physical_memory(start, end - start, offset);
    return 0;
}

static int fdt_entry_assign_irq(const uint32_t *entry, uint32_t cells)
{
    struct kvm_assigned_irq kvmirq;
    static const uint32_t mpic_to_kvm_flags[4] = {
        KVM_SYSIRQ_EDGE | KVM_SYSIRQ_HIGH,
        KVM_SYSIRQ_LEVEL | KVM_SYSIRQ_LOW,
        KVM_SYSIRQ_LEVEL | KVM_SYSIRQ_HIGH,
        KVM_SYSIRQ_EDGE | KVM_SYSIRQ_LOW
    };

    kvmirq.assigned_dev_id = 0;
    kvmirq.sysirq.intspec_len = cells;
    memcpy(kvmirq.sysirq.intspec, entry, cells * 4);
    kvmirq.flags =
        mpic_to_kvm_flags[(entry[1]) & 3] |
        KVM_DEV_IRQ_GUEST_SYSTEM | KVM_DEV_IRQ_HOST_SYSTEM |
        KVM_SYSIRQ_DEVTREE_INTSPEC;

    return kvm_assign_irq(&kvmirq);
}

static void fdt_intmap_assign_irqs(void *tree, int node)
{
#ifdef KVM_CAP_SYSTEM_IRQ_ASSIGNMENT
    const uint32_t *ints, *ncells, *intmap;
    uint32_t mpic_phandle;
    int len, intlen, mpic, ret, i;
    int child_cells = 0, mpic_addrcells, mpic_intcells;
    char path[MAX_DT_PATH];

    intmap = fdt_getprop(tree, node, "interrupt-map", &intlen);
    if (!intmap) {
        return;
    }

    qemu_devtree_get_path(tree, node, path, sizeof(path));

    ncells = fdt_getprop(tree, node, "#address-cells", &len);
    if (!ncells || len != 4) {
        fprintf(stderr, "%s: no #address-cells in %s\n",
                __func__, path);
        return;
    }
    child_cells += *ncells;

    ncells = fdt_getprop(tree, node, "#interrupt-cells", &len);
    if (!ncells || len != 4) {
        fprintf(stderr, "%s: no #interrupt-cells in %s\n",
                __func__, path);
        return;
    }
    child_cells += *ncells;

    mpic = fdt_node_offset_by_compatible(tree, -1, "chrp,open-pic");
    if (mpic < 0) {
        fprintf(stderr, "%s: no chrp,open-pic in guest device tree\n",
                __func__);
        return;
    }

    mpic_phandle = fdt_get_phandle(tree, mpic);
    if (!mpic_phandle) {
        fprintf(stderr, "%s: mpic node has no phandle\n", __func__);
        return;
    }

    ncells = fdt_getprop(tree, mpic, "#address-cells", &len);
    if (!ncells || len != 4) {
        fprintf(stderr, "%s: no #address-cells in the mpic node\n",
                __func__);
        return;
    }
    mpic_addrcells = *ncells;

    ncells = fdt_getprop(tree, mpic, "#interrupt-cells", &len);
    if (!ncells || len != 4) {
        fprintf(stderr, "%s: no #interrupt-cells in the mpic node\n",
                __func__);
        return;
    }
    if (*ncells > KVM_SYSIRQ_MAX_INTSPEC) {
        fprintf(stderr, "%s: #interrupt-cells %u exceeds maximum %d\n",
                __func__, *ncells, KVM_SYSIRQ_MAX_INTSPEC);
        return;
    }
    mpic_intcells = *ncells;

    for (i = 0, ints = intmap; (ints - intmap) * 4 < intlen; i++) {
        ints += child_cells;
        if (*ints != mpic_phandle) {
            /* Not necessarily an error, if it's for another interrupt
             * controller that is directly assigned or emulated.  But diagnose
             * it for now, as it could also be due to missing interrupt-parent
             * support, or a problem with the guest device tree.
             */
            fprintf(stderr, "%s: Couldn't assign interrupts (index %d) "
                    "from interrupt-map in %s: not mpic\n",
                    __func__, i, path);
            ints += mpic_addrcells + mpic_intcells + 1;
            continue;
        }

        ints += mpic_addrcells + 1;
        ret = fdt_entry_assign_irq(ints, mpic_intcells);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't assign irq %u (index %d) "
                    "from interrupt-map in %s: %s\n",
                    __func__, *ints, i, path, strerror(-ret));
        }

        ints += mpic_intcells;
    }
#endif
}

static void fdt_assign_irqs(void *tree, int node)
{
#ifdef KVM_CAP_SYSTEM_IRQ_ASSIGNMENT
    const uint32_t *ints, *intparent, *ncells;
    uint32_t mpic_phandle;
    int len, intlen, mpic, ret, i, ispec_bytes;
    int ip_node; /* node containing interrupt-parent */
    char path[MAX_DT_PATH];

    qemu_devtree_get_path(tree, node, path, sizeof(path));

    ints = fdt_getprop(tree, node, "interrupts", &intlen);
    if (!ints) {
        return;
    }

    for (ip_node = node, intparent = NULL; ip_node >= 0 && !intparent;
         ip_node = fdt_parent_offset(tree, ip_node)) {
        intparent = fdt_getprop(tree, ip_node, "interrupt-parent", &len);
    }

    if (!intparent) {
        fprintf(stderr, "%s: Couldn't find interrupt-parent for %s\n",
                __func__, path);
        return;
    }

    mpic = fdt_node_offset_by_compatible(tree, -1, "chrp,open-pic");
    if (mpic < 0) {
        fprintf(stderr, "%s: no chrp,open-pic in guest device tree\n",
                __func__);
        return;
    }

    mpic_phandle = fdt_get_phandle(tree, mpic);
    if (!mpic_phandle) {
        fprintf(stderr, "%s: mpic node has no phandle\n", __func__);
        return;
    }

    if (*intparent != mpic_phandle) {
        /* Not necessarily an error, if it's for another interrupt
         * controller that is directly assigned or emulated.  But diagnose
         * it for now, as it could also be due to missing interrupt-parent
         * support, or a problem with the guest device tree.
         */
        fprintf(stderr, "%s: Couldn't assign interrupts in %s: not mpic\n",
                __func__, path);
        return;
    }

    ncells = fdt_getprop(tree, mpic, "#interrupt-cells", &len);
    if (!ncells || len != 4) {
        fprintf(stderr, "%s: no #interrupt-cells in the mpic node\n",
                __func__);
        return;
    }

    if (*ncells > KVM_SYSIRQ_MAX_INTSPEC) {
        fprintf(stderr, "%s: #interrupt-cells %u exceeds maximum %d\n",
                __func__, *ncells, KVM_SYSIRQ_MAX_INTSPEC);
        return;
    }

    ispec_bytes = *ncells * 4;

    for (i = 0; i < intlen / ispec_bytes; i++) {
        ret = fdt_entry_assign_irq(&ints[i * *ncells], *ncells);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't assign irq %u (index %d) in %s: %s\n",
                    __func__, ints[i * *ncells], i, path, strerror(-ret));
        }
    }

    if (i * ispec_bytes != intlen) {
        fprintf(stderr, "%s: extra bytes at end of interrupts in %s\n",
                __func__, path);
    }
#endif
}

static void fdt_map_node(void *tree, int node, ram_addr_t *ramsize,
                         target_phys_addr_t *ram_bottom)
{
    int ret, len, dtlen;
    uint32_t naddr, nsize;
    const uint32_t *reg;
    unsigned int i;
    char path[MAX_DT_PATH];
    char *suffix;
    int path_len, suffix_len;
    const char *type;
    unsigned int is_ram = 0;
    int devmem;

    qemu_devtree_get_path(tree, node, path, sizeof(path));
    path_len = strlen(path) - 1;
    suffix = path + path_len;
    suffix_len = MAX_DT_PATH - path_len;

    reg = fdt_getprop(tree, node, "reg", &len);
    if (!reg) {
        /* Not an error -- could be specified as direct for the interrupts */
        return;
    }

    type = fdt_getprop(tree, node, "device_type", &dtlen);
    if (type && dtlen == strlen("memory") + 1 && !strcmp(type, "memory")) {
        is_ram = 1;
    }

    devmem = qemu_open("/dev/mem", O_RDWR);
    if (devmem < 0) {
        fprintf(stderr, "%s: Couldn't open /dev/mem for %s: %s\n",
                __func__, path, strerror(errno));
        return;
    }

    int parent = fdt_parent_offset(tree, node);
    if (parent < 0) {
        goto err;
    }

    ret = get_addr_format_nozero(tree, parent, &naddr, &nsize);
    if (ret < 0) {
        goto err;
    }

    for (i = 0; (unsigned int)len >= (naddr + nsize) * 4 * (i + 1); i++) {
        target_phys_addr_t addr, size;

        ret = xlate_reg(tree, node, &reg[(naddr + nsize) * i], &addr, &size,
                        naddr, nsize);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't get reg entry %u in %s\n",
                    __func__, i, path);
            continue;
        }

        snprintf(suffix, suffix_len, ":reg%d", i);

        ret = map_mem_from_file(devmem, addr, size, path);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't map reg entry %u in %s: %s\n",
                    __func__, i, path, strerror(-ret));
            continue;
        }

        if (is_ram && *ram_bottom > addr) {
            *ram_bottom = addr;
            *ramsize = size;
        }
    }

    close(devmem);
    return;

err:
    close(devmem);
    fprintf(stderr, "%s: Couldn't lookup reg in %s\n", __func__, path);
}

static void fdt_map_ranges(void *tree, int node)
{
    int ret, len;
    uint32_t naddr, nsize; /* parent #cells */
    uint32_t cnaddr, cnsize; /* child #cells */
    uint32_t rcells; /* #cells per range entry */
    const uint32_t *reg;
    unsigned int i;
    char path[MAX_DT_PATH];
    char *suffix;
    int path_len, suffix_len;
    int devmem;

    qemu_devtree_get_path(tree, node, path, sizeof(path));
    path_len = strlen(path) - 1;
    suffix = path + path_len;
    suffix_len = MAX_DT_PATH - path_len;

    reg = fdt_getprop(tree, node, "ranges", &len);
    if (!reg) {
        fprintf(stderr, "%s: qemu,direct-map-ranges but no ranges in %s\n",
               __func__, path);

        return;
    }

    devmem = qemu_open("/dev/mem", O_RDWR);
    if (devmem < 0) {
        fprintf(stderr, "%s: Couldn't open /dev/mem for %s: %s\n",
                __func__, path, strerror(errno));
        return;
    }

    int parent = fdt_parent_offset(tree, node);
    if (parent < 0) {
        goto err;
    }

    if (get_addr_format_nozero(tree, node, &cnaddr, &cnsize) < 0 ||
        get_addr_format_nozero(tree, parent, &naddr, &nsize) < 0) {
        goto err;
    }

    rcells = cnaddr + naddr + cnsize;

    for (i = 0; (unsigned int)len >= rcells * 4 * (i + 1); i++) {
        target_phys_addr_t addr, size;

        ret = xlate_reg(tree, node, &reg[rcells * i + cnaddr], &addr, &size,
                        naddr, cnsize);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't translate range %u in %s\n",
                    __func__, i, path);
            continue;
        }

        snprintf(suffix, suffix_len, ":range%d", i);

        ret = map_mem_from_file(devmem, addr, size, path);
        if (ret < 0) {
            fprintf(stderr, "%s: Couldn't map range %u in %s: %s\n",
                    __func__, i, path, strerror(ret));
            continue;
        }

    }

    close(devmem);
    return;

err:
    close(devmem);
    fprintf(stderr, "%s: Couldn't lookup ranges in %s\n", __func__, path);
}

void fdt_setup_direct_maps(void *fdt,  ram_addr_t *ramsize,
                           target_phys_addr_t *ram_bottom)
{
    int offset = -1;

    while ((offset = fdt_next_node(fdt, offset, NULL)) >= 0) {
        int len;

        if (fdt_getprop(fdt, offset, "qemu,direct-map", &len)) {
            fdt_map_node(fdt, offset, ramsize, ram_bottom);

            if (kvm_enabled()) {
                fdt_assign_irqs(fdt, offset);
                fdt_intmap_assign_irqs(fdt, offset);
            }
        }

        if (fdt_getprop(fdt, offset, "qemu,direct-map-ranges", &len)) {
            fdt_map_ranges(fdt, offset);
        }
    }
}

void fdt_setup_ram(void *fdt, ram_addr_t *ramsize,
                   target_phys_addr_t *ram_bottom)
{
    uint32_t mem_reg_property[MAX_ADDR_CELLS + MAX_SIZE_CELLS];
    int offset = -1, i;
    uint32_t addr_cells, size_cells, mem_reg_size;
    uint64_t size = *ramsize;

    /* check #address-cels, #size-cells and setup mem_reg_property */
    offset = fdt_path_offset(fdt, "/");
    if (get_addr_format_nozero(fdt, offset, &addr_cells, &size_cells) < 0) {
        fprintf(stderr, "failed to get address/size cells from root node\n");
    }

    mem_reg_size = (addr_cells + size_cells) * sizeof(uint32_t);
    memset(mem_reg_property, 0, mem_reg_size);
    for (i = size_cells; i > 0; i--) {
        mem_reg_property[addr_cells + i - 1] = cpu_to_be32((uint32_t)size);
        size = size >> 32;
    }

    while ((offset = fdt_node_offset_by_prop_value(fdt, offset, "device_type",
                                                   "memory", 7)) >= 0) {
        int len;

        if (fdt_getprop(fdt, offset, "qemu,direct-map", &len)) {
            continue;
        }

        /* Manipulate device tree in memory.  The inplace function
         * will fail if the existing reg is of a different size --
         * useful for catching #address-cells/#size-cells mismatch.
         */
        if (fdt_setprop_inplace(fdt, offset, "reg", mem_reg_property,
                                mem_reg_size) < 0) {
            fprintf(stderr, "couldn't set memory reg, check #cells\n");
        }

        *ram_bottom = 0;

        /* We only support one non-direct memory node */
        return;
    }

    /* Didn't find a non-direct memory node, so don't provide general RAM */
    *ramsize = 0;
}
