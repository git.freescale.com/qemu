/*
 * QEMU PowerPC direct memory map for guest definitions
 *
 * Copyright (C) 2011 Freescale Semiconductor, Inc.
 *
 * Author: Ashish Kalra     <ashish.kalra@freescale.com>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of  the GNU General  Public License as published by
 * the Free Software Foundation;  either version 2 of the  License, or
 * (at your option) any later version.
 */

#if !defined(DIRECT_MAP_H)
#define DIRECT_MAP_H

void fdt_setup_direct_maps(void *fdt,  ram_addr_t *ramsize, target_phys_addr_t *ram_bottom);
void fdt_setup_ram(void *fdt, ram_addr_t *ramsize, target_phys_addr_t *ram_bottom);

#endif /* !defined(DIRECT_MAP_H) */
