/*
 * QEMU board emulation for an e500v2-based virtual machine
 *
 * Copyright (C) 2009-2011 Freescale Semiconductor, Inc.
 *
 * Author: Yu Liu,     <yu.liu@freescale.com>
 *         Scott Wood  <scottwood@freescale.com>
 *
 * This file is derived from hw/ppc440_bamboo.c,
 * the copyright for that material belongs to the original owners.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of  the GNU General  Public License as published by
 * the Free Software Foundation;  either version 2 of the  License, or
 * (at your option) any later version.
 */

#include "config.h"
#include "qemu-common.h"
#include "net.h"
#include "hw.h"
#include "pc.h"
#include "pci.h"
#include "boards.h"
#include "sysemu.h"
#include "kvm.h"
#include "kvm_ppc.h"
#include "device_tree.h"
#include "openpic.h"
#include "ppc.h"
#include "uboot_image.h"
#include "loader.h"
#include "elf.h"
#include "sysbus.h"
#include "exec-memory.h"
#include "direct_map.h"
#include "host-utils.h"
#include "usb-ohci.h"

#define BINARY_DEVICE_TREE_FILE    "ppce500v2.dtb"
#define DTC_LOAD_PAD               0x500000
#define DTC_PAD_MASK               0xFFFFF
#define INITRD_LOAD_PAD            0x2000000
#define INITRD_PAD_MASK            0xFFFFFF

#define RAM_SIZES_ALIGN            (64UL << 20)

#define PPCE500V2_CCSRBAR_BASE       0xE0000000
#define PPCE500V2_MPIC_REGS_BASE     (PPCE500V2_CCSRBAR_BASE + 0x40000)
#define PPCE500V2_SERIAL0_REGS_BASE  (PPCE500V2_CCSRBAR_BASE + 0x4500)
#define PPCE500V2_SERIAL1_REGS_BASE  (PPCE500V2_CCSRBAR_BASE + 0x4600)
#define PPCE500V2_PCI_REGS_BASE      (PPCE500V2_CCSRBAR_BASE + 0x8000)
#define PPCE500V2_PCI_REGS_SIZE      0x1000
#define PPCE500V2_PCI_IO             0xE1000000
#define PPCE500V2_PCI_IOLEN          0x10000
#define PPCE500V2_UTIL_BASE          (PPCE500V2_CCSRBAR_BASE + 0xe0000)
#define PPCE500V2_SPIN_BASE          0xEF000000

struct boot_info
{
    target_phys_addr_t ram_start;
    target_phys_addr_t ram_end;
    target_phys_addr_t fdt_start;
    target_phys_addr_t fdt_end;
    target_phys_addr_t kernel_start;
    target_phys_addr_t kernel_end;
    target_phys_addr_t entry;
};

static void *ppce500v2_load_device_tree(CPUState *env, ram_addr_t *ramsize,
                                      const char *kernel_cmdline,
                                      int *fdt_size,
                                      target_phys_addr_t *ram_bottom,
                                      int *alloc_ram)
{
#ifdef CONFIG_FDT
    char *filename;
    void *fdt;
    uint8_t hypercall[16];
    ram_addr_t mapped_ramsize = 0;
    uint32_t clock_freq = 400000000;
    uint32_t tb_freq = 400000000;
    int i;

    if (dtb_file) {
        fdt = load_device_tree(dtb_file, fdt_size);
    } else {
        filename = qemu_find_file(QEMU_FILE_TYPE_BIOS, BINARY_DEVICE_TREE_FILE);
        if (!filename) {
            return NULL;
        }
        fdt = load_device_tree(filename, fdt_size);
        g_free(filename);
    }

    if (fdt == NULL) {
        return NULL;
    }

    fdt_setup_direct_maps(fdt, &mapped_ramsize, ram_bottom);
    fdt_setup_ram(fdt, ramsize, ram_bottom);

    if (*ramsize == 0) {
        *ramsize = mapped_ramsize;
        *alloc_ram = 0;
    } else {
        *alloc_ram = 1;
    }

    if (qemu_devtree_setprop_string(fdt, "/chosen", "bootargs",
                                    kernel_cmdline)) {
        fprintf(stderr, "couldn't set /chosen/bootargs\n");
    }

    if (kvm_enabled()) {
        /* Read out host's frequencies */
        clock_freq = kvmppc_get_clockfreq();
        tb_freq = kvmppc_get_tbfreq();

        /* indicate KVM hypercall interface */
        qemu_devtree_setprop_string(fdt, "/hypervisor", "compatible",
                                    "linux,kvm");
        kvmppc_get_hypercall(env, hypercall, sizeof(hypercall));
        qemu_devtree_setprop(fdt, "/hypervisor", "hcall-instructions",
                             hypercall, sizeof(hypercall));

        /* add "has-idle" prop if KVM supports ePAPR idle */
        if (kvmppc_get_hasidle(env)) {
            qemu_devtree_setprop(fdt, "/hypervisor", "has-idle", NULL, 0);
        }
    }

    /* We need to generate the cpu nodes in reverse order, so Linux can pick
       the first node as boot node and be happy */
    for (i = smp_cpus - 1; i >= 0; i--) {
        char cpu_name[128];
        uint64_t cpu_release_addr = cpu_to_be64(PPCE500V2_SPIN_BASE + (i * 0x20));

        for (env = first_cpu; env != NULL; env = env->next_cpu) {
            if (env->cpu_index == i) {
                break;
            }
        }

        if (!env) {
            continue;
        }

        snprintf(cpu_name, sizeof(cpu_name), "/cpus/PowerPC,8544@%x", env->cpu_index);
        qemu_devtree_add_subnode(fdt, cpu_name);
        qemu_devtree_setprop_cell(fdt, cpu_name, "clock-frequency", clock_freq);
        qemu_devtree_setprop_cell(fdt, cpu_name, "timebase-frequency", tb_freq);
        qemu_devtree_setprop_string(fdt, cpu_name, "device_type", "cpu");
        qemu_devtree_setprop_cell(fdt, cpu_name, "reg", env->cpu_index);
        qemu_devtree_setprop_cell(fdt, cpu_name, "d-cache-line-size",
                                  env->dcache_line_size);
        qemu_devtree_setprop_cell(fdt, cpu_name, "i-cache-line-size",
                                  env->icache_line_size);
        qemu_devtree_setprop_cell(fdt, cpu_name, "d-cache-size", 0x8000);
        qemu_devtree_setprop_cell(fdt, cpu_name, "i-cache-size", 0x8000);
        qemu_devtree_setprop_cell(fdt, cpu_name, "bus-frequency", 0);
        if (env->cpu_index) {
            qemu_devtree_setprop_string(fdt, cpu_name, "status", "disabled");
            qemu_devtree_setprop_string(fdt, cpu_name, "enable-method", "spin-table");
            qemu_devtree_setprop(fdt, cpu_name, "cpu-release-addr",
                                 &cpu_release_addr, sizeof(cpu_release_addr));
        } else {
            qemu_devtree_setprop_string(fdt, cpu_name, "status", "okay");
        }
    }

    return fdt;
#else
    return NULL;
#endif
}

static void ppce500v2_cpu_reset_sec(void *opaque)
{
    CPUState *env = opaque;

    cpu_reset(env);

    /* Secondary CPU starts in halted state for now. Needs to change when
       implementing non-kernel boot. */
    env->halted = 1;
    env->exception_index = EXCP_HLT;
}

static void ppce500v2_cpu_reset(void *opaque)
{
    CPUState *env = opaque;
    struct boot_info *bi = env->load_info;
    target_phys_addr_t ima_start, ima_end;

    cpu_reset(env);

    ima_start = MIN(bi->kernel_start, bi->fdt_start);
    ima_end = MAX(bi->kernel_end, bi->fdt_end);

    fsl_booke_epapr_init(env, bi->entry, bi->fdt_start, ima_start, ima_end,
                         bi->ram_start, bi->ram_end);
}

static void ppce500v2_init(ram_addr_t ram_size,
                         const char *boot_device,
                         const char *kernel_filename,
                         const char *kernel_cmdline,
                         const char *initrd_filename,
                         const char *cpu_model)
{
    MemoryRegion *address_space_mem = get_system_memory();
    MemoryRegion *ram = g_new(MemoryRegion, 1);
    PCIBus *pci_bus;
    CPUState *env = NULL;
    uint64_t elf_entry;
    uint64_t elf_lowaddr;
    target_phys_addr_t initrd_base = 0;
    target_long initrd_size=0;
    int i=0;
    unsigned int pci_irq_nrs[4] = {1, 2, 3, 4};
    qemu_irq **irqs, *mpic;
    DeviceState *dev;
    CPUState *firstenv = NULL;
    struct boot_info *bi = NULL;
    void *fdt;
    int fdt_size;
    int alloc_ram = 0;

    /* Setup CPUs */
    if (cpu_model == NULL) {
        cpu_model = "e500v2_v30";
    }

    irqs = g_malloc0(smp_cpus * sizeof(qemu_irq *));
    irqs[0] = g_malloc0(smp_cpus * sizeof(qemu_irq) * OPENPIC_OUTPUT_NB);
    for (i = 0; i < smp_cpus; i++) {
        qemu_irq *input;
        env = cpu_ppc_init(cpu_model);
        if (!env) {
            fprintf(stderr, "Unable to initialize CPU!\n");
            exit(1);
        }

        if (!firstenv) {
            firstenv = env;
        }

        irqs[i] = irqs[0] + (i * OPENPIC_OUTPUT_NB);
        input = (qemu_irq *)env->irq_inputs;
        irqs[i][OPENPIC_OUTPUT_INT] = input[PPCE500_INPUT_INT];
        irqs[i][OPENPIC_OUTPUT_CINT] = input[PPCE500_INPUT_CINT];
        env->spr[SPR_BOOKE_PIR] = env->cpu_index = i;

        ppc_booke_timers_init(env, 400000000, PPC_TIMER_E500);

        /* Register reset handler */
        if (!i) {
            /* Primary CPU */
            bi = g_malloc0(sizeof(struct boot_info));
            qemu_register_reset(ppce500v2_cpu_reset, env);
            env->load_info = bi;
        } else {
            /* Secondary CPUs */
            qemu_register_reset(ppce500v2_cpu_reset_sec, env);
        }
    }

    env = firstenv;

    /* Fixup Memory size on a alignment boundary */
    ram_size &= ~(RAM_SIZES_ALIGN - 1);

    bi->ram_start = ~(target_phys_addr_t)0;

    fdt = ppce500v2_load_device_tree(env, &ram_size, kernel_cmdline, &fdt_size,
                                   &bi->ram_start, &alloc_ram);
    if (!fdt) {
        fprintf(stderr, "Unable to load device tree.\n");
        exit(1);
    }

    bi->ram_end = bi->ram_start + ram_size - 1;

    if (reserve_perfmon) {
        kvmppc_assign_perfmon(env, fdt);
    }

    if (alloc_ram) {
        /* Register Memory */
        memory_region_init_ram(ram, NULL, "ppce500v2.ram", ram_size);
        memory_region_add_subregion(address_space_mem, 0, ram);
    }

    /* MPIC */
    mpic = mpic_init(address_space_mem, PPCE500V2_MPIC_REGS_BASE,
                     smp_cpus, irqs, NULL);

    if (!mpic) {
        cpu_abort(env, "MPIC failed to initialize\n");
    }

    /* Serial */
    if (serial_hds[0]) {
        serial_mm_init(address_space_mem, PPCE500V2_SERIAL0_REGS_BASE,
                       0, mpic[16+26], 399193,
                       serial_hds[0], DEVICE_BIG_ENDIAN);
    }

    if (serial_hds[1]) {
        serial_mm_init(address_space_mem, PPCE500V2_SERIAL1_REGS_BASE,
                       0, mpic[16+26], 399193,
                       serial_hds[0], DEVICE_BIG_ENDIAN);
    }

    /* General Utility device */
    sysbus_create_simple("mpc8544-guts", PPCE500V2_UTIL_BASE, NULL);

    /* PCI */
    dev = sysbus_create_varargs("e500-pcihost", PPCE500V2_PCI_REGS_BASE,
                                mpic[pci_irq_nrs[0]], mpic[pci_irq_nrs[1]],
                                mpic[pci_irq_nrs[2]], mpic[pci_irq_nrs[3]],
                                NULL);
    pci_bus = (PCIBus *)qdev_get_child_bus(dev, "pci.0");
    if (!pci_bus)
        printf("couldn't create PCI controller!\n");

    isa_mmio_init(PPCE500V2_PCI_IO, PPCE500V2_PCI_IOLEN);

    if (pci_bus) {
        /* Register network interfaces. */
        for (i = 0; i < nb_nics; i++) {
            pci_nic_init_nofail(&nd_table[i], "virtio", NULL);
        }

        if (usb_enabled) {
            usb_ohci_init_pci(pci_bus, -1);
        }
    }

    /* Register spinning region */
    sysbus_create_simple("e500-spin", PPCE500V2_SPIN_BASE, NULL);

    /* Needed for "fixed" address image loading */
    bi->kernel_start = bi->ram_start;

    /* Don't leave garbage in bi if we're not loading things */
    bi->kernel_end = bi->ram_end;
    bi->fdt_start = bi->ram_start;
    bi->fdt_end = bi->ram_start;
    bi->entry = bi->ram_start;

    /* Load kernel. */
    if (kernel_filename) {
        uboot_image_header_t hdr;
        int fixed = 1;  /* load addr doesn't come from uImage */
        int ret;

        if (get_uimage_header(kernel_filename, &hdr) != -1) {
            if (hdr.ih_load >= bi->ram_start &&
                hdr.ih_load + hdr.ih_size <= bi->ram_end)
                    fixed = 0;   /* use the addr from the uimage */
        }

        ret = load_uimage2(kernel_filename, &bi->entry,
                           &bi->kernel_start, NULL, fixed);

        if (ret < 0) {
            ret = load_elf(kernel_filename, NULL, NULL, &elf_entry,
                           &elf_lowaddr, NULL, 1, ELF_MACHINE, 0);
            bi->entry = elf_entry;
            bi->kernel_start = elf_lowaddr;
        }
        /* XXX try again as binary */
        if (ret < 0) {
            fprintf(stderr, "qemu: could not load kernel '%s'\n",
                    kernel_filename);
            exit(1);
        }

        bi->kernel_end = bi->kernel_start + ret - 1;
    }

    /* Load initrd. */
    if (initrd_filename) {
        initrd_base = (bi->kernel_end + 1 + INITRD_LOAD_PAD) &
                      ~INITRD_PAD_MASK;
        initrd_size = load_image_targphys(initrd_filename, initrd_base,
                      ram_size - initrd_base + bi->kernel_start);

        if (initrd_size < 0) {
            fprintf(stderr, "qemu: could not load initial ram disk '%s'\n",
                    initrd_filename);
            exit(1);
        }

#ifdef CONFIG_FDT
        if (fdt) {
            if (qemu_devtree_setprop_cell(fdt, "/chosen", "linux,initrd-start",
                                          initrd_base)) {
                fprintf(stderr, "couldn't set /chosen/linux,initrd-start\n");
            }

            if (qemu_devtree_setprop_cell(fdt, "/chosen", "linux,initrd-end",
                                          initrd_base + initrd_size)) {
                fprintf(stderr, "couldn't set /chosen/linux,initrd-end\n");
            }
        }
#endif
    }

    /* If we're loading a kernel directly, we must load the device tree too. */
    if (kernel_filename) {
#ifndef CONFIG_FDT
        cpu_abort(env, "Compiled without FDT support - can't load kernel\n");
#else
        if (!fdt) {
            fprintf(stderr, "couldn't load device tree\n");
            exit(1);
        }

        bi->fdt_start = (bi->kernel_end + 1 + DTC_LOAD_PAD) &
                        ~DTC_PAD_MASK;
        bi->fdt_end = bi->fdt_start + fdt_size - 1;

        rom_add_blob_fixed(BINARY_DEVICE_TREE_FILE, fdt,
                           fdt_size, bi->fdt_start);
        g_free(fdt);
#endif
    }

    if (kvm_enabled()) {
        kvmppc_init();
#ifdef KVM_CAP_SET_GUEST_DEBUG
        kvmppc_e500_hw_breakpoint_init();
#endif
    }
}

static QEMUMachine ppce500v2_machine = {
    .name = "ppce500v2",
    .desc = "ppce500v2",
    .init = ppce500v2_init,
    .max_cpus = 15,
};

static void ppce500v2_machine_init(void)
{
    qemu_register_machine(&ppce500v2_machine);
}

machine_init(ppce500v2_machine_init);
