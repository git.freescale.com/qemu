/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * Copyright IBM Corp. 2008
 * Copyright (C) 2010-2011 Freescale Semiconductor, Inc.
 *
 * Authors: Hollis Blanchard <hollisb@us.ibm.com>
 */

#ifndef __POWERPC_KVM_PARA_H__
#define __POWERPC_KVM_PARA_H__

#include <linux/types.h>

/*
 * Additions to this struct must only occur at the end, and should be
 * accompanied by a KVM_MAGIC_FEAT flag to advertise that they are present
 * (albeit not necessarily relevant to the current target hardware platform).
 *
 * Struct fields are always 32 or 64 bit aligned, depending on them being 32
 * or 64 bit wide respectively.
 *
 * See Documentation/virtual/kvm/ppc-pv.txt
 */
struct kvm_vcpu_arch_shared {
	__u64 scratch1;
	__u64 scratch2;
	__u64 scratch3;
	__u64 critical;		/* Guest may not get interrupts if == r1 */
	__u64 sprg0;
	__u64 sprg1;
	__u64 sprg2;
	__u64 sprg3;
	__u64 srr0;
	__u64 srr1;
	__u64 dar;		/* dear on BookE */
	__u64 msr;
	__u32 dsisr;
	__u32 int_pending;	/* Tells the guest if we have an interrupt */
	__u32 sr[16];
	__u32 mas0;
	__u32 mas1;
	__u64 mas7_3;
	__u64 mas2;
	__u32 mas4;
	__u32 mas6;
	__u32 esr;
	__u32 pir;

	/*
	 * SPRG4-7 are user-readable, so we can only keep these consistent
	 * between the shared area and the real registers when there's an
	 * intervening exit to KVM.  This also applies to SPRG3 on some
	 * chips.
	 *
	 * This suffices for access by guest userspace, since in PR-mode
	 * KVM, an exit must occur when changing the guest's MSR[PR].
	 * If the guest kernel writes to SPRG3-7 via the shared area, it
	 * must also use the shared area for reading while in kernel space.
	 */
	__u64 sprg4;
	__u64 sprg5;
	__u64 sprg6;
	__u64 sprg7;

	__u32 epr;

	/*
	 * mpic_prio_pending acts like int_pending -- enter hv if
	 * you set mpic_ctpr to be less than mpic_prio_pending.
	 *
	 * You may receive a spurious interrupt if MSR[EE]=0, you raise
	 * mpic_ctpr to be at least mpic_prio_pending, and you then set
	 * MSR[EE]=1 before lowering mpic_ctpr to be less than
	 * mpic_prio_pending.  If you want to avoid this, enter the
	 * hv any time you set ctpr and mpic_prio_pending is non-zero.
	 */
	__u32 mpic_ctpr, mpic_prio_pending;
};

#define KVM_SC_MAGIC_R0		0x4b564d21 /* "KVM!" */

#include <asm/epapr_hcalls.h>

/* ePAPR Hypercall Vendor ID */
#define HC_VENDOR_EPAPR		(EV_EPAPR_VENDOR_ID << 16)
#define HC_VENDOR_KVM		(EV_KVM_VENDOR_ID << 16)

/* ePAPR Hypercall Token */
#define HC_EV_IDLE		EV_IDLE

/* ePAPR Hypercall Return Codes */
#define HC_EV_SUCCESS		0
#define HC_EV_UNIMPLEMENTED	EV_UNIMPLEMENTED

#define KVM_FEATURE_MAGIC_PAGE	1

#define KVM_MAGIC_FEAT_SR		(1 << 0)

/* MASn, ESR, PIR, and high SPRGs */
#define KVM_MAGIC_FEAT_MAS0_TO_SPRG7	(1 << 1)

/* If set, paravirt EPR is used even if real hardware doesn't have it */
#define KVM_MAGIC_FEAT_EPR		(1 << 2)

/* Mask interrupts by priority */
#define KVM_MAGIC_FEAT_MPIC_CTPR	(1 << 3)


#endif /* __POWERPC_KVM_PARA_H__ */
